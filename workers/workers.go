package workers

import (
	"fmt"
	"github.com/go-redis/redis"
	"log"
	"sort"
	"strings"
	"sync"
)

func sortString(w string) string {
	s := strings.Split(w, "")
	sort.Strings(s)
	return strings.Join(s, "")
}

func Do(id int, jobs <-chan string, wg *sync.WaitGroup, conn *redis.ClusterClient) {
	defer wg.Done()
	for j := range jobs {
		fmt.Println("worker", id, "started  job", j)
		w := sortString(j)

		_, err := conn.ZIncrBy("words", 1, w).Result()
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("worker", id, "finished job", j, ", sorted word is", w)
	}
}
