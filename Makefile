.PHONY: default
default: build run
	## Builds the app and runs it.
	
.PHONY: godoc
godoc: ## Runs local godoc server.
	@echo "Running godoc server at http://localhost:8889"
	@godoc -http=:8889

.PHONY: build
build: ## Builds app binary.
	GOOS=linux go build -a .

.PHONY: run
run: ## Runs the app
	./redisWordCounter
