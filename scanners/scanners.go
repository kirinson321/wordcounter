package scanners

import (
	"github.com/go-redis/redis"
	"log"
)

type WordCount struct {
	Word string `json:"word"`
	Count int `json:"count"`
}


func GetResult(conn *redis.ClusterClient, results chan <- WordCount) {
	word, err := conn.ZRevRange("words", 0 ,0).Result()
	if err != nil {
		log.Fatal(err)
	}

	value, err := conn.ZScore("words", word[0]).Result()
	if err != nil {
		log.Fatal(err)
	}

	results <- WordCount{Word:word[0], Count:int(value)}
}
