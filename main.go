package main

import (
	"bufio"
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/kirinson321/wordcounter/scanners"
	"gitlab.com/kirinson321/wordcounter/workers"
	"log"
	"os"
	"sync"
)

func newRedisClusterClient() *redis.ClusterClient{
	 client := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: []string{":7000", ":7001", ":7002", ":7003", ":7004", ":7005"},
	})

	return client
}

func main() {
	var wg sync.WaitGroup
	jobs := make(chan string)
	results := make(chan scanners.WordCount)

	conn := newRedisClusterClient()

	filename := "testData"
	inputFile, err := os.Open(filename)
	if err != nil {
		fmt.Print("File testData not found in directory, please provide" +
			" valid text file name: ")
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		filename = scanner.Text()
		inputFile, err = os.Open(filename)
		if err != nil {
			log.Fatal(err)
		}
	}
	defer inputFile.Close()

	for w := 1; w <= 2; w++ {
		wg.Add(1)
		go workers.Do(w, jobs, &wg, conn)
	}

	scanner := bufio.NewScanner(inputFile)
	scanner.Split(bufio.ScanWords)
	fmt.Println("Waiting for all workers to finish...")
	for scanner.Scan() {
		jobs <- scanner.Text()
	}
	close(jobs)
	wg.Wait()

	go scanners.GetResult(conn, results)
	topWord := <- results
	fmt.Println("Word", topWord.Word, "had the highest count of",
		topWord.Count)

	close(results)

	err = conn.Close()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("main finished")
}
